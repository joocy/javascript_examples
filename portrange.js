(function() {

    function PortRange(min, max) {
      function initialisePortList(size) {
        var portlist = [];
        for (var i = 0; i < size; i++) {
          portlist[i] = true;
        }
        return portlist;
      }
      this.size = (max - min) + 1;
      this.ports = initialisePortList(this.size);
      this.min = min;
    }
    PortRange.prototype.acquirePort = function() {
       for (var i = 0; i < this.size; i++) {
         if (this.ports[i]) {
           this.ports[i] = false;
           return i + this.min;
         }
       }
       return -1;
    }
    PortRange.prototype.releasePort = function(port) {
      this.ports[port - this.min] = true;
    }

    function CheckedPortRange(min, max) {
        PortRange.call(this, min, max);
    }
    CheckedPortRange.prototype.nextFreePort = function() {
        for (var i = 0; i < this.size; i++) {
            if (this.ports[i]) {
                return i + this.min;
            }
        }
        return -1;
    }
    CheckedPortRange.prototype.isPortFree = function(port) {
        return this.ports[port - this.min];
    }

}());
