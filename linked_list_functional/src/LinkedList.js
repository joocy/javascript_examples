function Node(value, previous, next) {
    this.value = value;
    this.previous = previous;
    this.next = next;
}

function LinkedList() {
    this.size = 0;
    this.root = new Node();
}

function getNodeAt(node, index) {
    for (var i=0; i<=index; i++) {
        node = node.next;
    }
    return node;
}

LinkedList.prototype.add = function(value) {
    this.size++;
    var current = this.root;
    while (current.next !== undefined) {
        current = current.next;
    }
    current.next = new Node(value, current);
};

LinkedList.prototype.remove = function(index) {
    this.size--;
    var node = getNodeAt(this.root, index);
    var previous = node.previous;
    var next = node.next;
    previous.next = next;
    if (next !== undefined) next.previous = previous;
};

LinkedList.prototype.get = function(index) {
    return getNodeAt(this.root, index).value;
};

LinkedList.prototype.map = function(func) {
    var newLinkedList = new LinkedList();
    var node = this.root;
    while (node.next !== undefined) {
        node = node.next;
        newLinkedList.add(func(node.value));
    }
    return newLinkedList;
};

LinkedList.prototype.filter = function(predicate) {
    var newLinkedList = new LinkedList();
    var node = this.root;
    while (node.next !== undefined) {
        node = node.next;
        if (predicate(node.value)) {
            newLinkedList.add(node.value);
        }
    }
    return newLinkedList;
};
