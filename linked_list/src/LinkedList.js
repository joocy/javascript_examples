function Node(value, previous, next) {
    this.value = value;
    this.previous = previous;
    this.next = next;
}

function LinkedList() {
    this.size = 0;
    this.root = new Node();
}

function getNodeAt(node, index) {
    for (var i=0; i<=index; i++) {
        node = node.next;
    }
    return node;
}

LinkedList.prototype.add = function(value) {
    this.size++;
    var current = this.root;
    while (current.next !== undefined) {
        current = current.next;
    }
    current.next = new Node(value, current);
};

LinkedList.prototype.remove = function(index) {
    this.size--;
    var node = getNodeAt(this.root, index);
    var previous = node.previous;
    var next = node.next;
    previous.next = next;
    if (next !== undefined) next.previous = previous;
};

LinkedList.prototype.get = function(index) {
    return getNodeAt(this.root, index).value;
};
