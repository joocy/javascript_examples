describe("My LinkedList", function() {

    var linkedList;

    beforeEach(function() {
        linkedList = new LinkedList();
    });

    function prepareLinkedListForTests() {
        linkedList.add("first value");
        linkedList.add(42);
    }

    it("should be empty when first created", function() {
        expect(linkedList.size).toBe(0);
    });

    it("should increment size if a value is added", function() {
        linkedList.add("first value");
        expect(linkedList.size).toBe(1);
        linkedList.add(42);
        expect(linkedList.size).toBe(2);
    });

    it("should decrement size if a value is removed", function() {
        prepareLinkedListForTests();
        linkedList.remove(0);
        expect(linkedList.size).toBe(1);
        linkedList.remove(0);
        expect(linkedList.size).toBe(0);
    });

    it("should retrieve a value given an index", function() {
        prepareLinkedListForTests();
        expect(linkedList.get(0)).toBe("first value");
        expect(linkedList.get(1)).toBe(42);
    });

    it("should remove a value given an index", function() {
        prepareLinkedListForTests();
        linkedList.remove(0);
        expect(linkedList.get(0)).toBe(42);
    });

});
